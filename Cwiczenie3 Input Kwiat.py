class Flower:
    def __init__(self, species, blossom_season, colour):
        self.species = species
        self.blossom_season = blossom_season
        self.colour = colour
        print("A {} {} is created. It blossoms in {}.".format(self.colour, self.species, self.blossom_season))

tulipan = Flower(input("What flower species is it: "), input("When does it blossom: "), input("What colour is it: "))