class Instrumenty(object):
    pass

class Dete(Instrumenty):
    pass

class Blaszane(Dete):
    pass

class Drewniane(Dete):
    pass

class Perkusyjne(Instrumenty):
    pass

class Membranofony(Perkusyjne):
    pass

class Idiofony(Perkusyjne):
    pass

class Strunowe(Instrumenty):
    pass

class Szarpane(Strunowe):
    pass

class Smyczkowe(Strunowe):
    pass

class Mloteczkowe(Strunowe):
    pass

puzon = Blaszane()
saksofon = Drewniane()
bebny = Membranofony()
trojkat = Idiofony()
gitara = Szarpane()
skrzypce = Smyczkowe()
fortepian = Mloteczkowe()


print(isinstance(puzon, Instrumenty))
print(isinstance(puzon, Dete))
print(isinstance(puzon, Drewniane))
print(isinstance(puzon, Blaszane))
print(isinstance(fortepian, Blaszane))
print(isinstance(gitara, Strunowe))
print(isinstance(gitara, Szarpane))