class Human(object):
    def __init__(self, firstname, surname, sex):
        self.firstname = firstname
        self.surname = surname
        self.sex = sex
        print("Rodzi sie {} o imieniu i nazwisku {} {}.".format(self.sex, self.firstname,self.surname))

    def __del__(self):
        print("{} o imieniu i nazwisku {} {} umiera.".format(self.sex, self.firstname, self.surname))


class Teacher(Human):
    def Teach(self):
        print("{} {} uczy jezyka angielskiego.".format(self.firstname, self.surname))

class JudoTrainer(Teacher):
    def __init__(self, firstname, surname, sex, belt):
        super(JudoTrainer, self).__init__(firstname, surname, sex)
        self.belt = belt
        print("{} {} ma {} pas.".format(self.firstname, self.surname, self.belt))

    def Train(self):
        print("{} {} prowadzi treningi judo.".format(self.firstname, self.surname))

aurelka = Teacher("Aurelia", "Durawa", "kobieta")
aurelka.Teach()
del aurelka
slawek = JudoTrainer("Slawomir", "Durawa", "mezczyzna", "czarny")
slawek.Train()
slawek.Teach()


